#!/usr/bin/env python
# pylint: disable=unused-argument

import logging
import re
import requests
from urllib.parse import urljoin, urlparse
from bs4 import BeautifulSoup
from telegram import Update, InputMediaPhoto
from telegram.ext import (
    Application,
    ChatMemberHandler,
    CommandHandler,
    MessageHandler,
    filters,
    ContextTypes,
    CallbackContext,
)
import time
import asyncio
import os
import yt_dlp
from aiogram import Bot, Dispatcher, types
from io import BytesIO
import subprocess

# Token del bot (debes reemplazar esto con tu token real)
TOKEN = os.environ.get('TOKEN')

# Configuración de logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


class ImperioScraper:
    def __init__(self, base_url):
        self.base_url = base_url.rstrip("/")
        self.headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
            "Referer": self.base_url,
        }
        self.session = requests.Session()

    def sanitize_filename(self, name):
        """Sanitiza nombres de archivo."""
        return re.sub(r'[\\/*?:"<>|]', "", name)

    def extract_image_links(self, url):
        """Extrae enlaces de imágenes de todas las páginas de la galería, recorriendo la paginación."""
        image_links = []
        next_page = url
        dominios_permitidos = ["imperiodefamosas.com"]

        while next_page:
            try:
                response = self.session.get(next_page, headers=self.headers)
                if response.status_code != 200:
                    logger.warning(f"Error {response.status_code} al acceder a {next_page}")
                    break

                soup = BeautifulSoup(response.text, "html.parser")
                gallery_div = soup.find('div', id='galeria')
                if not gallery_div:
                    break

                # Extraemos los enlaces de imágenes de la página actual
                photo_links = gallery_div.select('div#photos a[href]')
                image_links.extend([urljoin(next_page, a["href"]) for a in photo_links])

                # Obtener la siguiente página utilizando el método get_next_page
                next_page = self.get_next_page(soup, next_page)

            except Exception as e:
                logger.error(f"Error al extraer enlaces de imágenes: {e}")
                break
            
            
        image_links_filtrados = [link for link in image_links if self.es_dominio_valido(link, dominios_permitidos)]

        return image_links_filtrados

    def get_next_page(self, soup, current_url):
            # Método 1: Paginación numérica automática
            parsed_url = urlparse(current_url)
            path_parts = parsed_url.path.split('/')
            
            if path_parts[-1].isdigit():
                next_num = int(path_parts[-1]) + 1
                next_url = '/'.join(path_parts[:-1] + [str(next_num)])
            else:
                next_url = parsed_url.path + '/2'
            
            next_page = urljoin(current_url, next_url)
            if self.validate_next_page(next_page):
                return next_page
            
            # Método 2: Detección HTML de paginación
            pagination = soup.find('div', class_='text-center')
            if pagination:
                for ul in pagination.find_all('ul', class_='pagination'):
                    next_li = ul.find('li', class_='next')
                    if next_li and 'disabled' not in next_li.get('class', []):
                        next_link = next_li.find('a')
                        if next_link and next_link.has_attr('href'):
                            next_page = urljoin(current_url, next_link['href'])
                            if self.validate_next_page(next_page):
                                return next_page
            return None

    def validate_next_page(self, url):
            try:
                response = self.session.head(url, headers=self.headers)
                return response.status_code == 200
            except:
                return False
            
    def dividir_en_lotes(self, lista, tamano_lote=10):
        """Generador que divide una lista en lotes de tamaño fijo."""
        for i in range(0, len(lista), tamano_lote):
            yield lista[i:i + tamano_lote]
    
    def es_dominio_valido(self, url, dominios_permitidos):
        """Verifica si la URL pertenece a un dominio permitido."""
        dominio = urlparse(url).netloc  # Extrae el dominio del enlace
        return any(dominio.endswith(permitido) for permitido in dominios_permitidos)

async def descargar_y_enviar_imagenes(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if not context.args:
        await update.message.reply_text("Por favor, proporciona una URL de una página con imágenes.")
        return

    
    url = context.args[0]
    scraper = ImperioScraper(url)

    parsed_url = urlparse(url)
    last_part = parsed_url.path.rstrip("/").split("/")[-1].replace("-", "")

    await update.effective_chat.send_message(
        f"#{last_part}"
    )

    try:
        
        # Extraer enlaces de imágenes de la página actual
        image_links = scraper.extract_image_links(url)
        if not image_links:
            await update.message.reply_text("No se encontraron imágenes en la página.")
        
        batch_size = 10  # Tamaño del lote

        #for lote in scraper.dividir_en_lotes(image_links, batch_size):
        #    image_batch = []  # Lista para almacenar imágenes del lote actual
        for img_url in image_links:   
            
            
            #for img_url in lote:
                # image_response = scraper.session.get(img_url, headers=scraper.headers, stream=True, timeout=6000)
                
                #if image_response.status_code == 200:
                #image_batch.append(InputMediaPhoto(img_url))
                #else:
                #    logger.warning(f"Error {image_response.status_code} al descargar la imagen: {img_url}")
            await update.effective_chat.send_photo(img_url)
            await asyncio.sleep(5)
                    
            # Enviar el lote si hay imágenes en él
            #if image_batch:
            #    await update.message.reply_media_group(media=image_batch)
            #    await asyncio.sleep(5)

        await update.effective_chat.send_message(
            f"#{last_part}"
        )
        await context.bot.delete_message(update.message.chat_id, update.message.message_id)
                    

    except Exception as e:
        logger.error(f"Error al extraer imágenes: {e}")

async def download_video(update: Update, context: CallbackContext) -> None:
    url = context.args[0]
    await update.message.reply_text('Downloading the video...')

    ydl_opts = {
        'format': 'bestvideo+bestaudio/best',
        'outtmpl': 'downloaded_video.%(ext)s',
        'merge_output_format': 'mkv',
    }

    try:
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            info_dict = ydl.extract_info(url, download=True)
            video_path = ydl.prepare_filename(info_dict)
            audio_path = video_path.replace('.mp4', '.m4a')

        ffmpeg_path = os.path.join(os.getcwd(), 'bin', 'ffmpeg')

        merged_path = 'merged_video.mp4'
        ffmpeg_command = [
            ffmpeg_path, '-i', video_path, '-i', audio_path, '-c:v', 'libx264', '-c:a', 'aac', '-strict', 'experimental', '-movflags', '+faststart', merged_path
        ]
        subprocess.run(ffmpeg_command, check=True)

        with open(merged_path, 'rb') as video_file:
            await context.bot.send_video(chat_id=update.message.chat_id, video=video_file)

        await update.message.reply_text('Video downloaded and sent!')
    except Exception as e:
        logger.error(f"Error downloading video: {e}")
        await update.message.reply_text('An error occurred while downloading the video.')

async def ola(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Responde con 'Ola'."""
    await update.effective_chat.send_message(
        f"Ola {update.effective_user.mention_html()}!",
        parse_mode="HTML",
    )


async def responder_mensaje(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Responde a cada mensaje que no sea un comando y controla lenguaje ofensivo."""
    mensaje = update.message.text.lower()

    # Lista de palabras ofensivas
    palabras_ofensivas = ["mierda", "caca", "idiota"]

    if any(palabra in mensaje for palabra in palabras_ofensivas):
        try:
            await context.bot.delete_message(update.message.chat_id, update.message.message_id)
            await update.effective_chat.send_message("⚠️ Tu mensaje fue eliminado por lenguaje inapropiado.")
        except Exception as e:
            logger.warning(f"No se pudo eliminar el mensaje: {e}")
    else:
        await update.effective_chat.send_message("Hola crack! ¡Que tengas un excelente día!")


def main() -> None:
    """Punto de entrada del bot."""
    application = Application.builder().token(TOKEN).build()

    # Handlers
    application.add_handler(CommandHandler("ola", ola))
    application.add_handler(CommandHandler("if", descargar_y_enviar_imagenes))
    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, responder_mensaje))
    application.add_handler(CommandHandler("vk", download_video))

    # Iniciar el bot
    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()